﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OnRailsShooter
{
    public class ORSRandomRotation : MonoBehaviour
    {
        public float range = 360;
        public float offset = 0;

        // Use this for initialization
        void Start()
        {
            transform.eulerAngles = Vector3.up * (offset + Random.Range(0, range));
        }
    }

}
