﻿using OnRailsShooter;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public enum Difficult
    {
        Easy,
        Normal,
        Hard
    }

    public Difficult difficult;

    private void Start()
    {
        switch (difficult)
        {
            case Difficult.Easy:
                player.health = 20;
                player.weapons[0].damage = 5;
                break;

            case Difficult.Normal:
                player.health = 15;
                player.weapons[0].damage = 2;
                break;

            case Difficult.Hard:
                player.health = 10;
                player.weapons[0].damage = 1;
                break;
        }
    }

    [Header("Dependencies:")]
    public Text healthText;
    public ORSPlayer player;

    private void Update()
    {
        healthText.text = "x" + player.health.ToString();
    }
}
